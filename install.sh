#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        ____                       _   _                    ##
##                  _ __ |___ \ ___  _ __ ___   __ _| |_| |_                  ##
##                 | '_ \  __) / _ \| '_ ` _ \ / _` | __| __|                 ##
##                 | | | |/ __/ (_) | | | | | | (_| | |_| |_                  ##
##                 |_| |_|_____\___/|_| |_| |_|\__,_|\__|\__|                 ##
##                              www.n2omatt.com                               ##
##  File      : install.sh                                                    ##
##  Project   : blog_utils                                                    ##
##  Date      : May 19, 2018                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : n2omatt <n2omatt@druid.games>                                 ##
##  Copyright : n2omatt - 2018                                                ##
##                                                                            ##
##  Description :                                                             ##
##    Installation script for blog_utils.                                     ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/druid_games/shellscript_utils.sh


##----------------------------------------------------------------------------##
## Variables                                                                  ##
##----------------------------------------------------------------------------##
SCRIPT_DIR=$(druid_get_script_dir);
SRC_DIR="$SCRIPT_DIR/src";
INSTALL_DIR=/usr/local/src/n2omatt/blog_utils
BASHRC="$HOME/.bashrc";

INCLUDE_FILE="$INSTALL_DIR/blog_utils.sh";
MARK_INSTALL_LINE="## n2omatt's blog_utils - www.n2omatt.com";
MARK_CMD_LINE="[[ -s \"$INCLUDE_FILE\" ]] && source \"$INCLUDE_FILE\"";


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
## Install all source files in the destination directory.
mkdir -pv "$INSTALL_DIR";
for ITEM in $(ls "$SRC_DIR"); do
    cp -vf $SRC_DIR/$ITEM $INSTALL_DIR;
done;

##------------------------------------------------------------------------------
## Update the .bashrc to source the installed files.
##   So the functions can be used directly.
TEMP1=$(mktemp);
TEMP2=$(mktemp);

## Remove the previous source instructions.
cat "$BASHRC" | grep --invert-match "$MARK_INSTALL_LINE" > "$TEMP1";
cat "$TEMP2"  | grep --invert-match "$MARK_CMD_LINE"     > "$TEMP2";

# ## Add the new ones.
echo "$MARK_INSTALL_LINE" >> "$TEMP2";
echo "$MARK_CMD_LINE"     >> "$TEMP2";

## Clean the temps and set the .bashrc
mv "$TEMP2" "$BASHRC";
rm "$TEMP1";

echo "Done...";
