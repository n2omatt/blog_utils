#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        ____                       _   _                    ##
##                  _ __ |___ \ ___  _ __ ___   __ _| |_| |_                  ##
##                 | '_ \  __) / _ \| '_ ` _ \ / _` | __| __|                 ##
##                 | | | |/ __/ (_) | | | | | | (_| | |_| |_                  ##
##                 |_| |_|_____\___/|_| |_| |_|\__,_|\__|\__|                 ##
##                              www.n2omatt.com                               ##
##  File      : blog_utils.sh                                                 ##
##  Project   : blog_utils                                                    ##
##  Date      : May 19, 2018                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : n2omatt <n2omatt@druid.games>                                 ##
##  Copyright : n2omatt - 2018                                                ##
##                                                                            ##
##  Description :                                                             ##
##    An umbrella "include" file for all the blog_utils functions.            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/druid_games/shellscript_utils.sh


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
## "Include" (source) all the files bellow. This will make the user able
## to call any of the blog_utils functions, without the need to install
## them on something like /usr/local/bin and without messing with the
## users path.
SCRIPT_DIR=$(druid_get_script_dir);

source "$SCRIPT_DIR/create_post.sh"
