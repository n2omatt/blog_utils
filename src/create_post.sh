#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        ____                       _   _                    ##
##                  _ __ |___ \ ___  _ __ ___   __ _| |_| |_                  ##
##                 | '_ \  __) / _ \| '_ ` _ \ / _` | __| __|                 ##
##                 | | | |/ __/ (_) | | | | | | (_| | |_| |_                  ##
##                 |_| |_|_____\___/|_| |_| |_|\__,_|\__|\__|                 ##
##                              www.n2omatt.com                               ##
##  File      : ceate_post.sh                                                 ##
##  Project   : blog_utils                                                    ##
##  Date      : May 19, 2018                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : n2omatt <n2omatt@druid.games>                                 ##
##  Copyright : n2omatt - 2018                                                ##
##                                                                            ##
##  Description :                                                             ##
##    Create a post with the name as the given args in the current dir.       ##
##    Example:                                                                ##
##      $blog_create_post Lets blog a lot                                     ##
##      -> YYYY-MM-DD-lets-blog-a-lot.md                                      ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Import                                                                     ##
##----------------------------------------------------------------------------##
source /usr/local/src/druid_games/shellscript_utils.sh


##----------------------------------------------------------------------------##
## Functions                                                                  ##
##----------------------------------------------------------------------------##
blog_create_post()
{
    ## Not title provided...
    if [ $# == 0 ]; then
        echo "Missing blog post name...";
        return 1;
    fi;

    ## Get the date of the post.
    local CURR_DATE_FILENAME=$(date --rfc-3339="date");
    local CURR_DATE_FRONT_MATTER=$(date --rfc-3339="seconds");

    ## Get the title of the post.
    local RAW_TITLE="$@";
    local CLEAN_TITLE="";
    for WORD in $RAW_TITLE; do
        local CLEAN_WORD=$(echo "$WORD" | tr -cd [:alnum:] | tr [:upper:] [:lower:]);
        CLEAN_TITLE+="$CLEAN_WORD ";
    done
    CLEAN_TITLE=$(druid_trim_right "$CLEAN_TITLE");

    ## Build the filename.
    local FILENAME="$CURR_DATE_FILENAME $CLEAN_TITLE.md";
    local CLEAN_FILENAME=$(echo $FILENAME | sed s/" "/"-"/g);

    ## Build the front matter.
    local FRONT_MATTER=$(cat <<EOF
---
layout: post
title:  "$RAW_TITLE"
date:   $CURR_DATE_FRONT_MATTER
categories:
---
EOF
)
    ## Debug....
    # echo "RAW_TITLE      : $RAW_TITLE";
    # echo "CLEAN_TITLE    : $CLEAN_TITLE";
    # echo "FILENAME       : $FILENAME";
    # echo "CLEAN_FILENAME : $CLEAN_FILENAME";
    # echo "FRONT_MATTER   : $FRONT_MATTER"

    ## Log.
    echo "--> Creating post:";
    echo "    Name: $(druid_FY $CLEAN_TITLE)";
    echo "    File: $(druid_FC $CLEAN_FILENAME)";
    echo "    Path: $(druid_FM "$PWD")";

    ## Does not overwrite previous files.
    if [ -e "$CLEAN_FILENAME" ]; then
        echo $(druid_FR "Already has a post with this filename. Aborting...");
        return 1;
    fi;

    echo "$FRONT_MATTER" > "$CLEAN_FILENAME";
    echo "Done..."
}
